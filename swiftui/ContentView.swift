//
//  ContentView.swift
//  swiftui
//
//  Created by Shinto Joseph on 09/12/19.
//  Copyright © 2019 Shinto Joseph. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World! The SwiftUi starts")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
